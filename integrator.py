#!/usr/local/bin/python3

import argparse

from confile_checkator import *
from incrementor import *
from buildator import *
from uploadator import *
from credential_manager import *
import initializer


#
# Helping functions
#


def integrator_print(msg):
    print("[Integrator]: {}".format(msg))

#
# End helpers
#


parser = argparse.ArgumentParser(description="Continius integration made in Python by a super internship ! \\o/", prog='INTEGRATOR')
parser.add_argument('--init', action='store_true', help='Creates the confile file.')
parser.add_argument('--skip-build', action='store_true', help='Makes Integrator skip the build/archive phase.')
parser.add_argument('--init-credentials', metavar='ACCOUNT', default=None, nargs=1, help='Creates keychain access to upload the app on iTunes Connect. It takes one argument, which is the user\'s account')
parser.add_argument('--confile', default='./confile', help='gives the confile path if different. (default is \'./confile\'')
parser.add_argument('--crashlytics', action='store_true', help='Makes Integrator uploading the application archive to Crashlytics')
parser.add_argument('--appstore', action='store_true', help='Makes Integrator uploading the application archive to iTunesConnect')
parser.add_argument('--version', action='version', version='%(prog)s 0.1 (beta)', help='Display the current version')

args = vars(parser.parse_args())
print(args)
confile_path = args['confile']
retVal = 0
init_only = False

if args['init'] is True:
    initializer.init_confile()
    init_only = True

if args['init_credentials'] is not None:
    credentials = CredentialManger(args['init_credentials'][0])
    credentials.get_pwd()
    init_only = True

if init_only is True:
    exit(0)

checkator = ConfileCheckator(confile_path, args)
main_config = checkator.check()


integrator_print("Integration has started...you cannot go back now...")

if args['skip_build'] is not True:
    # incrementing build numbers and version numbers
    incrementor = Incrementor(main_config["build_infos"])
    incrementor.increment()

    # build the project
    buildator = Buildator(main_config["project_infos"], args['crashlytics'], args['appstore'])
    retVal = buildator.build()

    if retVal != 0:
        exit(retVal)

# upload to Crashlytics or iTunesConnect
ipa_name = main_config["project_infos"]["name"] + ".ipa"
uploadator = Uploadator()
if args['crashlytics']:
    retVal = uploadator.upload_crashlytics(main_config["crashlytics"], ipa_name)

if retVal != 0:
    exit(retVal)

if args['appstore']:
    uploadator.upload_itunes(main_config["itunesconnect"], ipa_name)

print("[Integrator]: Integration done. You're just lucky...")
