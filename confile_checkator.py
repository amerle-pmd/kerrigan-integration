__author__ = 'Anthony'

import json


class ConfileCheckator:
    def __init__(self, confile_path, args):
        self.confile_path = confile_path
        self.crashlytics = args["crashlytics"]
        self.appstore = args["appstore"]

    @staticmethod
    def print(msg):
        print("[ConfileCheckator]: {}".format(msg))

    def check(self):
        self.print("Starting to check the given configuration at path {}".format(self.confile_path))
        with open(self.confile_path, 'r') as confile:
            config = json.load(confile)

        # Check project's informations
        assert "project_infos" in config
        project_infos = config["project_infos"]
        assert "name" in project_infos
        assert "type" in project_infos
        assert "config" in project_infos
        assert "default" in project_infos["config"]
        assert "scheme" in project_infos

        # Check build's informations
        assert "build_infos" in config
        build_infos = config["build_infos"]
        assert "targets" in build_infos
        for target_infos in build_infos["targets"]:
            assert "name" in target_infos

        # Check Crashlytics informations
        if self.crashlytics:
            assert "crashlytics" in config
            crash_config = config["crashlytics"]
            assert "api_key" in crash_config
            assert "build_secret" in crash_config

        # Check iTunesConnect
        if self.appstore:
            assert "itunesconnect" in config
            itunes_config = config["itunesconnect"]
            assert "account" in itunes_config

        self.print("Everything seems fine in your confile :)")
        return config