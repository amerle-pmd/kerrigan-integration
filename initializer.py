__author__ = 'Anthony'

import os
import json


def init_confile():
    print("Initializing integrator by creating the confile file")
    cwd = os.getcwd()

    json_confile = {
        "project_infos": {
            "name": "projectName",
            "type": "workspace",
            "config": {
                "crashlytics": "theCrashlyticsConfiguration",
                "appstore": "theAppstoreConfiguration",
                "default": "theDefaultConfiguration"
            },
            "scheme": "schemeName",
            "ipa": "ipaName"
        },
        "build_infos": {
            "version": "1.0",
            "targets": [
                {
                    "name": "targetName",
                    "path": "path/to/targetDirectory (optional)",
                    "plist_name": "name.plist (optional)"
                }
            ]
        },
        "crashlytics": {
            "api_key": "the_key",
            "build_secret": "the_secret",
            "framework_path": "path/to/Crashlytics.framework",
            "group_aliases": "some,aliases (optional)",
            "emails": "some,emails (optional)"
        },
        "itunesconnect": {
            "account": "the@account.com"
        }
    }

    with open(cwd + "/confile", "w") as confile:
        json.dump(json_confile, confile, indent=4)
    print("Initialization completed successfuly")