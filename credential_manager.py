__author__ = 'Anthony'

import keyring
import getpass

prefix_account = "integrator."


class CredentialManger:
    def __init__(self, account):
        self.print("Initializing with account : {}".format(account))
        self.user_account = account
        self.service = prefix_account + account
        self.keyring = keyring.get_keyring()
        self.pwd = None

    @staticmethod
    def print(msg):
        print("[Crediential Manager]: {}".format(msg))

    def has_credentials(self):
        self.pwd = self.keyring.get_password(self.service, self.user_account)
        if self.pwd is None:
            return False
        return True

    def get_pwd(self):
        if not self.has_credentials():
            pwd = getpass.getpass(prompt="Enter your credentials for account {}:\n".format(self.user_account))
            self.keyring.set_password(self.service, self.user_account, pwd)
            self.pwd = pwd
        return self.pwd