__author__ = 'Anthony'

from subprocess import call


class Buildator:
    def __init__(self, project_infos, crashlytics, appstore):
        self.project_infos = project_infos
        self.buildCrashlytics = crashlytics
        self.buildAppstore = appstore
        self.say_hello()

    @staticmethod
    def print(msg):
        print("[Buildator]: {}".format(msg))

    def say_hello(self):
        print("\n")
        self.print("Hello, I'll build your project {} in the configurations \"{}\" !".format(self.project_infos["name"], self.project_infos["config"]))

    def say_goodbye(self):
        self.print("Build success ! \\o")
        print("\n")

    def build(self):
        if self.project_infos["type"] == "workspace":
            project_type_flag = "-w"
            project_type = ".xcworkspace"
        elif self.project_infos["type"] == "project":
            project_type_flag = "-c"
            project_type = ".xcodeproj"
        else:
            raise ValueError("The project type should either be \"workspace\" or \"project\". Not {}".format(self.project_infos["type"]))
        project_path = self.project_infos["name"] + project_type
        self.print("Building at path \"{}\"".format(project_path))

        configuration = self.project_infos["config"]["default"]
        if self.buildCrashlytics:
            configuration = self.project_infos["config"]["crashlytics"]
        elif self.buildAppstore:
            configuration = self.project_infos["config"]["appstore"]


        call_args = ["ipa", "build", project_type_flag, project_path, "-c", configuration, "-s", self.project_infos["scheme"], "--verbose", "true"];
        self.print(call_args)
        ret = call(call_args)
        self.say_goodbye()
        return ret