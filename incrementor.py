__author__ = 'Anthony'

import os
import plistlib
from git import Repo

build_nbr_key = "CFBundleVersion"
app_version_key = "CFBundleShortVersionString"


class Incrementor:
    def __init__(self, build_infos):
        self.build_infos = build_infos
        self.say_hello()

    @staticmethod
    def print(msg):
        print("[Incrementor]: {}".format(msg))

    def say_hello(self):
        print("\n")
        self.print("Hello, I'll increment the build and version number of the following targets: {}!".format([info["name"] for info in self.build_infos["targets"]]))

    def say_goodbye(self):
        self.print("I've updated the version of your app as you wanted :3 See ya ! \\o")
        print("\n")

    @staticmethod
    def __info_plist_path(target, plist_name='Info.plist'):
        for root, dirs, files in os.walk("{}".format(target)):
            for name in files:
                if name == plist_name:
                    return "{}/{}".format(root, name)


    @staticmethod
    def __build_number():
        repo = Repo(os.getcwd())
        return str(len(list(repo.iter_commits())))

    def __version_number(self, target_infos):
        target_directory = target_infos["name"]
        if "path" in target_infos:
            target_directory = target_infos["path"]
        if "plist_name" in target_infos:
            plist_path = self.__info_plist_path(target_directory, target_infos["plist_name"])
        else:
            plist_path = self.__info_plist_path(target_directory)
        with open(plist_path, 'r+b') as plist_file:
            pl = plistlib.load(plist_file)
        return pl[app_version_key]

    @staticmethod
    def __set_version_for_plist_file(plist_path, version, build_number):
        with open(plist_path, 'r+b') as plist_file:
            pl = plistlib.load(plist_file)
        pl[app_version_key] = version
        pl[build_nbr_key] = build_number
        with open(plist_path, 'w+b') as plist_file:
            plistlib.dump(pl, plist_file)


    def increment(self):
        build_number = self.__build_number()
        self.print("The new build number is {}".format(build_number))
        version_number = self.__version_number(self.build_infos["targets"][0])
        self.print("The new version number is {}".format(version_number))
        for target_infos in self.build_infos["targets"]:
            self.print("Incrementing version number of target: {}".format(target_infos["name"]))
            target_directory = target_infos["name"]
            if "path" in target_infos:
                target_directory = target_infos["path"]
            if "plist_name" in target_infos:
                plist_path = self.__info_plist_path(target_directory, target_infos["plist_name"])
            else:
                plist_path = self.__info_plist_path(target_directory)
            self.__set_version_for_plist_file(plist_path, version_number, build_number)
            self.print("Successfully incrementied build number of target {} to {}".format(target_infos["name"], build_number))
        self.say_goodbye()