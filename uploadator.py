__author__ = 'Anthony'

from credential_manager import *
from subprocess import call


class Uploadator:
    @staticmethod
    def print(msg):
        print("[Uploadator]: {}".format(msg))

    def upload_crashlytics(self, params, ipa_name):
        print("\n")
        self.print("I'll send your project to Crashlytics")
        cmd = "./Crashlytics.framework/submit"
        if 'framework_path' in params:
            cmd = params["framework_path"] + "/Crashlytics.framework/submit"
        ipa_path = "./"
        if 'ipa_path' in params:
            ipa_path = params["ipa_path"]
        ipa_path = ipa_path + ipa_name

        ipa_call_args = [cmd, params["api_key"], params["build_secret"], "-ipaPath", ipa_path]

        if 'group_aliases' in params:
            ipa_call_args = ipa_call_args + ["-groupAliases", params["group_aliases"]]
        if 'emails' in params:
            ipa_call_args = ipa_call_args + ["-emails", params["emails"]]

        print(ipa_call_args)
        ret = call(ipa_call_args)
        self.print("Your project is on Crashlytics now :)")
        return ret


    # /Applications/Xcode.app/Contents/Applications/Application\ Loader.app/Contents/Frameworks/ITunesSoftwareService.framework/Support/altool
    #  --upload-app -f ./$archiveName -u adcprisma@prisma-presse.com -p $mdp
    def upload_itunes(self, params, ipa_name):
        self.print("I'll send your project on iTunesConnect")
        credentials = CredentialManger(params["account"])
        cmd = "/Applications/Xcode.app/Contents/Applications/Application Loader.app/Contents/Frameworks/ITunesSoftwareService.framework/Support/altool"
        call_args = [cmd, "--upload-app", "-f", ipa_name, "-u", credentials.user_account, "-p", credentials.get_pwd()]
        print(call_args)
        ret = call(call_args)
        self.print("Your project is on iTunesConnect now :)")
        return ret